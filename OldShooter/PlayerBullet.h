//
//  PlayerBullet.h
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__PlayerBullet__
#define __OldShooter__PlayerBullet__

#include <stdio.h>
#include "AliveGameObject.h"
#include "Game.h"
#include "Utils.h"

#define PLAYER_BULLET_SCALE 0.3
#define PLAYER_BULLET_HEALTH 1
#define PLAYER_BULLET_DAMAGE 1

// forward declaration to avoid circular references
class PlayerBullet : public AliveGameObject {
public:
    
    PlayerBullet (Game *game, GameObjectType gameObjectType, float x, float y, float angle)
    : AliveGameObject(SPACEGAME_SPRITESHEET,
                 "laserBlue02.png",
                 x,
                 y,
                 PLAYER_BULLET_SCALE,
                 PLAYER_BULLET_SCALE,
                 90.f,
                 gameObjectType,
                 game,
                 PLAYER_BULLET_HEALTH,
                 PLAYER_BULLET_DAMAGE) {
        _angle = angle;
    }
    
    void update(float deltaTime);
private:
    float _angle;
    
};

#endif /* defined(__OldShooter__PlayerBullet__) */
