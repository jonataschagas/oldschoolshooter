//
//  Node.cpp
//  OldShooter
//
//  Created by jchagas on 09/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "Node.h"
#include "GLUtil.h"
#include "Atlas2DManager.h"

Node::Node(string atlasName, string defaultClip, float x, float y, float scaleX, float scaleY, float rotateAngle){
    _currentClip = defaultClip;
    _atlas2D = Atlas2DManager::getAtlas(atlasName);
    _defaultClip = defaultClip;
    position.x = x;
    position.y = y;
    _scaleX = scaleX;
    _scaleY = scaleY;
    _width = _atlas2D->getQuadWidth(_defaultClip);
    _height = _atlas2D->getQuadHeight(_defaultClip);
    _rotateAngle = rotateAngle;
}

Node::~Node(){}

void Node::render() {
    _atlas2D->render(_currentClip, position.x, position.y, _scaleX, _scaleY, _rotateAngle);
}

void Node::rotate(int axis, float degrees) {
    
}

void Node::scale(int axis, float percentage) {
    
}

void Node::update(float deltaTime) {
    
}

void Node::addChild(Node *node) {
    
}

void Node::removeChild(Node *node) {
    
}

float Node::getWidth() {
    return _atlas2D->getQuadWidth(_currentClip) * _scaleX;
}

float Node::getHeight() {
    return _atlas2D->getQuadHeight(_currentClip) * _scaleY;
}

void Node::setCurrentClip(string clipName) {
    _currentClip = clipName;
}

string Node::getCurrentClip() {
    return _currentClip;
}