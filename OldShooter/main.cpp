//
//  main.cpp
//  OldShooter
//
//
// particle explosion: http://gamedev.stackexchange.com/questions/1679/fastest-way-to-create-a-simple-particle-effect
//
//
//  Created by Jonatas Chagas on 03/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include <string.h>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <GLUT/glut.h>
#include "Game.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include <sys/time.h>

// global vars  
float screen_width = 1024.0f;
float screen_height = 768.0f;
Game *game;
// FRAMES PER SECOND
float MS_PER_TICK = 1000/60;

using namespace std;
void draw() {
    
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    
    // renders the game
    game->render();

    // swap buffers (has to be done at the end)
    glutSwapBuffers();
    
}

float getCurrentTime() {
    timeval time;
    gettimeofday(&time, NULL);
    return (time.tv_sec * 1000) + (time.tv_usec / 1000);
}

float lastLoop = 0;
void update(int value) {
    float deltaTime = (glutGet(GLUT_ELAPSED_TIME) - lastLoop)/1000;
    
    // Call update() again in 'interval' milliseconds
    glutTimerFunc(MS_PER_TICK, update, 0);
    
    // updates the game
    game->update(deltaTime);
    
    // Redisplay frame
    glutPostRedisplay();
    
    lastLoop = glutGet(GLUT_ELAPSED_TIME);
}

void enable2D() {
    glViewport(0, 0, screen_width, screen_height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, screen_width, 0.0f, screen_height, 0.0f, 1.0f);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
}

int main(int argc, char** argv)
{
    
    game = Game::create(screen_width, screen_height);
    
    // initialize opengl (via glut)
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(screen_width, screen_height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Old School shooter");
    
    const GLubyte* renderer = glGetString(GL_RENDERER);
    const GLubyte* version = glGetString(GL_VERSION);
    printf ("Renderer: %s\n", renderer);
    printf ("OpenGL version supported %s\n", version);
    
    //Enable texturing
    glEnable( GL_TEXTURE_2D );
    
    //Set blending
    glEnable( GL_BLEND );
    glDisable( GL_DEPTH_TEST );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    
    //Initialize DevIL
    ilInit();
    ilClearColour( 255, 255, 255, 000 );
    ILenum ilError = ilGetError();
    if( ilError != IL_NO_ERROR )
    {
        printf( "Error initializing DevIL! %s\n", iluErrorString( ilError ) );
        return false;
    }
    
    // Register callback functions
    glutDisplayFunc(draw);
    glutTimerFunc(MS_PER_TICK, update, 0);
    
    // setup scene to 2d mode and set draw color to white
    enable2D();
    glColor3f(1.0f, 1.0f, 1.0f);
    
    // start the whole thing
    glutMainLoop();
    
    return 0;
}