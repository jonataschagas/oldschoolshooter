//
//  EnemyBullet.h
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__EnemyBullet__
#define __OldShooter__EnemyBullet__

#include <stdio.h>

#include <stdio.h>
#include "AliveGameObject.h"
#include "Game.h"
#include "Utils.h"

#define ENEMY_BULLET_SCALE 0.3
#define ENEMY_BULLET_HEALTH 1
#define ENEMY_BULLET_DAMAGE 10

// forward declaration to avoid circular references
class EnemyBullet : public AliveGameObject {
public:
    
    EnemyBullet (Game *game, GameObjectType gameObjectType, float x, float y)
    : AliveGameObject(SPACEGAME_SPRITESHEET,
                 "laserRed02.png",
                 x,
                 y,
                 ENEMY_BULLET_SCALE,
                 ENEMY_BULLET_SCALE,
                 90.f,
                 gameObjectType,
                 game,
                ENEMY_BULLET_HEALTH,
                ENEMY_BULLET_DAMAGE) {
    }
    
    void update(float deltaTime);
    
    
    float getWidth();
    float getHeight();
    
};

#endif /* defined(__OldShooter__EnemyBullet__) */
