//
//  Point.h
//  OldShooter
//
//  Created by jchagas on 17/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Point__
#define __OldShooter__Point__

#include <stdio.h>

class Vector;
class Point2D {
public:
    
    Point2D addVector(Vector v);
    
    float x,y;
};

#endif /* defined(__OldShooter__Point__) */
