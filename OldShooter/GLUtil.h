//
//  GLUtil.h
//  OldShooter
//
//  Created by jchagas on 11/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__GLUtil__
#define __OldShooter__GLUtil__

#include <stdio.h>
#include <GLUT/glut.h>
#include <sstream>
#include <fstream>
#include <string>
#include <IL/il.h>

using namespace std;

typedef struct TextureDTO {
    
    string path;
    GLuint width;
    GLuint height;
    GLuint textureId;
    bool initialized = false;
    
} TextureDTO;

class GLUtil {
public:
    static void color(float red, float green, float blue) {
        glColor3f(red, green, blue);
    }
    
    static void renderQuad(float x, float y, float z,float width, float height) {
        
        // unbinds the textures
        glBindTexture( GL_TEXTURE_2D, NULL );
        
        glPushMatrix();
        
        glTranslatef(x, y, z);
        
        // draws a quad
        glBegin(GL_QUADS);
        glVertex2f(x, y);
        glVertex2f(x + width, y);
        glVertex2f(x + width, y + height);
        glVertex2f(x, y + height);
        glEnd();
        
        glPopMatrix();
    };
    
    static void renderQuad(float x, float y, float z,float width, float height, float r, float g, float b) {
        
        // unbinds the textures
        glBindTexture( GL_TEXTURE_2D, NULL );
        
        glColor3f(r, g, b);
        
        glPushMatrix();
        
        glTranslatef(x, y, z);
        
        // draws a quad
        glBegin(GL_QUADS);
        glVertex2f(x, y);
        glVertex2f(x + width, y);
        glVertex2f(x + width, y + height);
        glVertex2f(x, y + height);
        glEnd();
        
        glPopMatrix();
    };

    
    static void drawText(const char* text, float x, float y) {
        // unbinds the textures
        glBindTexture( GL_TEXTURE_2D, NULL );
        // white color
        glColor3f(1.f, 1.f, 1.f);
        // converts int to string
        glRasterPos2f(x,y);
        int len = (int) strlen(text);
        for (int i = 0; i < len; i++) {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, text[i]);
        }
    }
    
    static TextureDTO loadTextureFromFile(string path) {
        //Generate and set current image ID
        ILuint imgID = 0;
        ilGenImages( 1, &imgID );
        ilBindImage( imgID );
        
        //Load image
        ILboolean success = ilLoadImage( path.c_str() );
        
        
        TextureDTO textureDTO;
        
        //Image loaded successfully
        if( success == IL_TRUE )
        {
            //Convert image to RGBA
            success = ilConvertImage( IL_RGBA, IL_UNSIGNED_BYTE );
            
            if( success == IL_TRUE )
            {
                textureDTO.width = (GLuint)ilGetInteger( IL_IMAGE_WIDTH );
                textureDTO.height = (GLuint)ilGetInteger( IL_IMAGE_HEIGHT );
                textureDTO.path = path;
                
                //Create texture from file pixels
                textureDTO.textureId = loadTextureFromPixels32( (GLuint*)ilGetData(), textureDTO.width, textureDTO.height );
                textureDTO.initialized = true;
                
                //Delete file from memory
                ilDeleteImages( 1, &imgID );
                
            }
            
        }
        
        return textureDTO;
    }
    
    static bool is_file_exist(const char *fileName)
    {
        std::ifstream infile(fileName);
        return infile.good();
    }
    
    static GLuint loadTextureFromPixels32( GLuint* pixels, GLuint width, GLuint height) {
        
        // color blending
        GLuint size = width * height;
        for( int i = 0; i < size; ++i )
        {
            //Get pixel colors
            GLubyte* colors = (GLubyte*)&pixels[ i ];
            
            //Color matches
            if( colors[ 0 ] == 0 && colors[ 1 ] == 0 && colors[ 2 ] == 0 && ( colors[ 3 ] == 0 ) )
            {
                //Make transparent
                colors[ 0 ] = 255;
                colors[ 1 ] = 255;
                colors[ 2 ] = 255;
                colors[ 3 ] = 000;
            }
        }
        
        GLuint textureId;
        
        //Generate texture ID
        glGenTextures( 1, &textureId );
        
        //Bind texture ID
        glBindTexture( GL_TEXTURE_2D, textureId );
        
        //Generate texture
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
        
        //Set texture parameters
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        
        //Unbind texture
        glBindTexture( GL_TEXTURE_2D, NULL );
        
        //Check for error
        GLenum error = glGetError();
        if( error != GL_NO_ERROR )
        {
            printf( "Error loading texture from %p pixels! %s\n", pixels, gluErrorString( error ) );
            return 0;
        }
        
        return textureId;
    }
    
    static void renderPoint(GLfloat x, GLfloat y) {
        glPushMatrix();
        
        // white color
        glColor3f(1.f, 1.f, 1.f);
        
        // draws a quad
        glBegin(GL_POINT);
        glVertex2f(x, y);
        glEnd();
        
        glPopMatrix();
    }
    
    static void renderQuadWithTexture( GLfloat x, GLfloat y, GLuint textureId, GLuint width, GLuint height) {
        //If the texture exists
        if( textureId != 0 )
        {
            //Remove any previous transformations
            glLoadIdentity();
            
            //Move to rendering point
            glTranslatef( x, y, 0.f );
            
            //Set texture ID
            glBindTexture( GL_TEXTURE_2D, textureId );
            
            // resetting the colors
            glColor4f(1.0, 1.0, 1.0, 1.0);
            
            //Render textured quad
            glBegin( GL_QUADS );
            glTexCoord2f( 0.f, 0.f ); glVertex2f(           0.f,            0.f );
            glTexCoord2f( 1.f, 0.f ); glVertex2f( width,            0.f );
            glTexCoord2f( 1.f, 1.f ); glVertex2f( width, height );
            glTexCoord2f( 0.f, 1.f ); glVertex2f(           0.f, height );
            glEnd();
        }
    }
    
    static void renderQuadWithTexture( GLfloat x, GLfloat y, GLuint textureId, GLuint width, GLuint height,
                                      GLfloat textX, GLfloat textY,
                                      GLfloat textWidth, GLfloat textHeight, GLfloat scaleX, GLfloat scaleY, GLfloat rotateAngle) {
        //If the texture exists
        if( textureId != 0 )
        {
            
            //Texture coordinates
            GLfloat texLeft = textX / width;
            GLfloat texRight = ( textX + textWidth ) / width;
            GLfloat texTop = textY / height;
            GLfloat texBottom = ( textY + textHeight ) / height;
            
            //Vertex coordinates
            GLfloat quadWidth = textWidth;
            GLfloat quadHeight = textHeight;
            
            //Remove any previous transformations
            glLoadIdentity();
            
            //Move to rendering point
            glTranslatef( x, y, 0.f );
            
            //Set texture ID
            glBindTexture( GL_TEXTURE_2D, textureId );
            
            // resetting the colors
            glColor4f(1.0, 1.0, 1.0, 1.0);
            
            glPushMatrix();
            
            glScalef(scaleX, scaleY, 0.f);
            
            glRotatef(rotateAngle, 0.f, 0.f, 1.f);
            
            //Render textured quad
            glBegin( GL_QUADS );
            glTexCoord2f( texLeft, texTop ); glVertex2f(           0.f,            0.f );
            glTexCoord2f( texRight, texTop ); glVertex2f( quadWidth,            0.f );
            glTexCoord2f( texRight, texBottom ); glVertex2f( quadWidth, quadHeight );
            glTexCoord2f( texLeft, texBottom ); glVertex2f(           0.f, quadHeight );
            glEnd();
            
            glPopMatrix();
        }
    }
    
    
};

#endif /* defined(__OldShooter__GLUtil__) */
