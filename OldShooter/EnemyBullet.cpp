//
//  EnemyBullet.cpp
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "EnemyBullet.h"


#define ENEMY_BULLET_SPEED 8;

void EnemyBullet::update(float deltaTime) {
    position.x -= ENEMY_BULLET_SPEED;
}

float EnemyBullet::getWidth() {
    return getWidth() * 2;
}

float EnemyBullet::getHeight() {
    return getHeight() * 2;
}