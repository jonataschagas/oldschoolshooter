//
//  Damageable.h
//  OldShooter
//
//  Created by jchagas on 25/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Damageable__
#define __OldShooter__Damageable__

#include <stdio.h>

class Damageable {
public:
    
    Damageable(float damage) {
        _damage = damage;
    }
    
    float getDamage() {
        return _damage;
    }
    
private:
    float _damage;
};

#endif /* defined(__OldShooter__Damageable__) */
