//
//  Atlas2DManager.h
//  OldShooter
//
//  Created by jchagas on 20/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Atlas2DManager__
#define __OldShooter__Atlas2DManager__

#include <stdio.h>
#include "Atlas2D.h"
#include <vector>

using namespace std;

class Atlas2DManager {
public:

    static void loadAtlas(string name, string path, string metaFilePath) {
        if(!_instance) {
            _instance = new Atlas2DManager();
        }
        Atlas2D *atlas = new Atlas2D(name, path, metaFilePath);
        _instance->_listOfAtlases.push_back(atlas);
    }
    
    static Atlas2D* getAtlas(string name) {
        for(int i = 0; i < _instance->_listOfAtlases.size(); i++) {
            if(strcmp(name.c_str(),_instance->_listOfAtlases.at(i)->getName().c_str()) == 0) {
                return _instance->_listOfAtlases.at(i);
            }
        }
        return nullptr;
    }
private:
    static Atlas2DManager *_instance;
    Atlas2DManager() {}
    vector<Atlas2D*> _listOfAtlases;
    

};

#endif /* defined(__OldShooter__Atlas2DManager__) */
