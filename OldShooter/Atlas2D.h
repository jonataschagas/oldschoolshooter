//
//  Atlas2D.h
//  OldShooter
//
//  Created by jchagas on 20/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Atlas2D__
#define __OldShooter__Atlas2D__

#include <stdio.h>
#include <string>
#include <vector>
#include "TextureClip2D.h"
#include <GLUT/glut.h>

using namespace std;

class Atlas2D {
public:

    Atlas2D(string name, string path, string metaFilePath);
    
    void load();
    
    void render(string clipName, float x, float y, float scaleX, float scaleY, float rotateAngle);
    
    string getName();
    
    float getWidth();
    
    float getHeight();
    
    float getQuadWidth(string clipName);
    
    float getQuadHeight(string clipName);
    
private:
    string _name;
    string _path;
    string _metaFilePath;
    float _height;
    float _width;
    GLuint _textureId;
    bool _initialized = false;
    vector<TextureClip2D*> _textureClips;
    
    TextureClip2D* getClipByName(string clipName);
    
};

#endif /* defined(__OldShooter__Atlas2D__) */
