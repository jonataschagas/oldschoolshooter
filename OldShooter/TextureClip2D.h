//
//  TextureClip2D.h
//  OldShooter
//
//  Created by jchagas on 19/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__TextureClip2D__
#define __OldShooter__TextureClip2D__

#include <stdio.h>
#include <string>

using namespace std;

class TextureClip2D {

public:
    
    TextureClip2D();
    
    TextureClip2D(string namep, float xp, float yp, float widthp, float heightp);
    
    string name;
    float width;
    float height;
    float x;
    float y;
    
};

#endif /* defined(__OldShooter__TextureClip2D__) */
