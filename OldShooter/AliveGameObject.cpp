//
//  AliveGameObject.cpp
//  OldShooter
//
//  Created by jchagas on 25/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "AliveGameObject.h"

AliveGameObject::AliveGameObject(string atlasName, string defaultClip, float x, float y, float scaleX, float scaleY, float rotateAngle
                , GameObjectType type, Game *game, float health, float damage)
: GameObject(atlasName, defaultClip, x, y, scaleX, scaleY, rotateAngle, type, game), Damageable(damage){
    _health = health;
}

float AliveGameObject::getHealth() {
    return _health;
}

void AliveGameObject::setDamageToHealth(float damage) {
    _health -= damage;
}

void AliveGameObject::setIncreaseHealth(float increase) {
    _health += increase;
    if(_health > 100) {
        _health = 100;
    }
}