//
//  SoundManager.cpp
//  OldShooter
//
//  Created by jchagas on 22/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "SoundManager.h"
#include "fmod.hpp"
#include <stdio.h>
#include "Utils.h"

SoundManager* SoundManager::m_pInstance = NULL;

SoundManager* SoundManager::Instance() {
    if (!m_pInstance) {  // Only allow one instance of class to be generated.
        m_pInstance = new SoundManager;
        m_pInstance->init();
    }
    return m_pInstance;
}

void SoundManager::init() {

    /*
     Create a System object and initialize
     */
    result = FMOD::System_Create(&system);
    
    result = system->getVersion(&version);
    
    if (version < FMOD_VERSION)
    {
        printf("FMOD lib version %08x doesn't match header version %08x", version, FMOD_VERSION);
    }
    
    result = system->init(32, FMOD_INIT_NORMAL, extradriverdata);
}

void SoundManager::playSound(string file) {
    void *buff = 0;
    int length = 0;
    FMOD_CREATESOUNDEXINFO exinfo;

    Utils::loadFileMemory(file.c_str(), &buff, &length);
    memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    exinfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
    exinfo.length = length;

    result = system->createSound((const char *)buff, FMOD_OPENMEMORY, &exinfo, &sound2);
    if (result != FMOD_OK) {
        printf("errror");
    }
}