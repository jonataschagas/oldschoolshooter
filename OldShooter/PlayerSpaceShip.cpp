//
//  PlayerSpaceShip.cpp
//  OldShooter
//
//  Created by jchagas on 09/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "PlayerSpaceShip.h"
#include "Game.h"
#include "GameObject.h"
#include "SoundManager.h"
#include "MathFunctions.h"

# define PLAYER_SPEED 5
# define EXTRA_AMMO_TIME 800
# define SHIELD_TIME 800

void PlayerSpaceShip::update(float deltaTime) {
    
    // This grabs all key states, then checks if you were holding down ⌘ or not
    GetKeys(_keyStates);
    
    if(isKeyDown(kVK_Return)) {
        if(getHealth() <= 0) {
            _game->resetGame();
        } else {
            _game->setStartGame();
        }
    }
    
    if(isKeyDown(kVK_DownArrow)) {
        _velocityGoal.y = -PLAYER_SPEED;
    } else if(isKeyDown(kVK_UpArrow)) {
        _velocityGoal.y = PLAYER_SPEED;
    } else {
        _velocityGoal.y = 0;
    }
    
    if(isKeyDown(kVK_LeftArrow)) {
        _velocityGoal.x = -PLAYER_SPEED;
    } else if(isKeyDown(kVK_RightArrow)) {
        _velocityGoal.x = PLAYER_SPEED;
    } else {
        _velocityGoal.x = 0;
    }
    
    _velocity.x = MathFunctions::lerp(_velocityGoal.x, _velocity.x, deltaTime * 30);
    _velocity.y = MathFunctions::lerp(_velocityGoal.y, _velocity.y, deltaTime * 30);
    
    position.x += _velocity.x;
    position.y += _velocity.y;
    
    if(position.x < _game->getScreenWidth() * 0.05f) {
        position.x = _game->getScreenWidth() * 0.05f;
    } else if(position.x > _game->getScreenWidth() * 0.95) {
        position.x = _game->getScreenWidth() * 0.95f;
    }
    
    if(position.y > _game->getScreenHeight() * 0.95) {
        position.y = _game->getScreenHeight() * 0.95;
    } else if(position.y < 10) {
        position.y = 10;
    }
    
    static float timeShooting = 0;
    if(isKeyDown(kVK_Space) && (timeShooting == 0 || timeShooting > 15)) {
        if(_extraAmmo) {
            _game->createBullets(GameObjectType::PLAYER_BULLET, position.x + _width/2, position.y + 12, 60.f);
            _game->createBullets(GameObjectType::PLAYER_BULLET, position.x + _width/2, position.y + 12, 30.f);
            _game->createBullets(GameObjectType::PLAYER_BULLET, position.x + _width/2, position.y + 12, 0.f);
            _game->createBullets(GameObjectType::PLAYER_BULLET, position.x + _width/2, position.y + 12, -30.f);
            _game->createBullets(GameObjectType::PLAYER_BULLET, position.x + _width/2, position.y + 12, -60.f);
            
        } else {
            _game->createBullets(GameObjectType::PLAYER_BULLET, position.x + _width/2, position.y + 12, 0.f);
        }
        
        timeShooting = 0;
    }
    timeShooting += 1;
    _extraAmmoTime += 1;
    _shieldTime += 1;
    
    // turns off the extra ammo
    if(_extraAmmo && _extraAmmoTime > EXTRA_AMMO_TIME) {
        _extraAmmo = false;
    }
    
    // turns off the shield
    if(_shield && _shieldTime > SHIELD_TIME) {
        _shield = false;
    }
    
}

bool PlayerSpaceShip::isKeyDown( uint16_t vKey )
{
    uint8_t index = vKey / 32 ;
    uint8_t shift = vKey % 32 ;
    return _keyStates[index].bigEndianValue & (1 << shift) ;
}

void PlayerSpaceShip::setExtraAmmo() {
    _extraAmmo = true;
    _extraAmmoTime = 0;
}

bool PlayerSpaceShip::isExtraAmmodOn() {
    return _extraAmmo;
}

void PlayerSpaceShip::setShield() {
    _shield = true;
    _shieldTime = 0;
}

bool PlayerSpaceShip::isShieldOn() {
    return _shield;
}
