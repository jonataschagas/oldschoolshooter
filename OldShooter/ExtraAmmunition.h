//
//  ExtraAmmunition.h
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__ExtraAmmunition__
#define __OldShooter__ExtraAmmunition__

#include <stdio.h>
#include "GameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"
#include <math.h>

#define SCALE 1
#define SPEED 3

/*
 The sine waves are described here: http://www.helixsoft.nl/articles/circle/sincos.htm
 */

class ExtraAmmunition : public GameObject {
public:
    
    ExtraAmmunition (Game *game, GameObjectType gameObjectType, float x, float y)
    : GameObject(SPACEGAME_SPRITESHEET,
                 "powerupBlue_bolt.png",
                 x,
                 y,
                 SCALE,
                 SCALE,
                 180.f,
                 gameObjectType,
                 game) {
    }
    
    
    void update(float deltaTime);
    
    float getWidth();
    float getHeight();
    
private:
};

#endif /* defined(__OldShooter__ExtraAmmunition__) */
