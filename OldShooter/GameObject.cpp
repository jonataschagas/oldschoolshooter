//
//  GameObject.cpp
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "GameObject.h"


GameObject::GameObject(string atlasName, string defaultClip, float x, float y, float scaleX, float scaleY, float rotateAngle,
                       GameObjectType type, Game *game)
    : Node(atlasName, defaultClip, x, y, scaleX, scaleY, rotateAngle){
    _type = type;
    _game = game;
}

GameObjectType GameObject::getType() {
    return _type;
}