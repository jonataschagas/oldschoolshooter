//
//  Atlas2D.cpp
//  OldShooter
//
//  Created by jchagas on 20/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "Atlas2D.h"
#include "GLUtil.h"
#include "Utils.h"
#include "rapidxml.hpp"
#include <stdlib.h>

using namespace rapidxml;

Atlas2D::Atlas2D(string name, string path, string metaFilePath) {
    _name = name;
    _path = path;
    _metaFilePath = metaFilePath;
}

void Atlas2D::load() {
    // loads the texture into the GPU
    TextureDTO textureDTO = GLUtil::loadTextureFromFile(_path);
    _textureId = textureDTO.textureId;
    _width = textureDTO.width;
    _height = textureDTO.height;
    
    // now we read the metafile and we load the clips
    string metaFileContents = Utils::readFileContents(_metaFilePath);
    char *cstr = new char[metaFileContents.length() + 1];
    strcpy(cstr, metaFileContents.c_str());
    
    xml_document<> doc;
    doc.parse<0>(cstr);
    xml_node<> *pRoot = doc.first_node();

    for(xml_node<> *pNode=pRoot->first_node(); pNode; pNode=pNode->next_sibling()) {
        char* name = pNode->name();
        if (strcmp(name, "SubTexture") == 0) {
            TextureClip2D *textureClip2D = new TextureClip2D();
            for (xml_attribute<> *attr = pNode->first_attribute(); attr; attr = attr->next_attribute()) {
                if(strcmp(attr->name(), "name") == 0) {
                    textureClip2D->name = attr->value();
                } else if(strcmp(attr->name(), "x") == 0) {
                    textureClip2D->x = strtof(attr->value(),NULL);
                } else if(strcmp(attr->name(), "y") == 0) {
                    textureClip2D->y = strtof(attr->value(),NULL);
                } else if(strcmp(attr->name(), "width") == 0) {
                    textureClip2D->width = strtof(attr->value(),NULL);
                } else if(strcmp(attr->name(), "height") == 0) {
                    textureClip2D->height = strtof(attr->value(),NULL);
                }
            }
            _textureClips.push_back(textureClip2D);
        }
    }
    _initialized = true;
}

void Atlas2D::render(string clipName, float x, float y, float scaleX, float scaleY, float rotateAngle) {
    if(_initialized == false) {
        load();
    }
    
    TextureClip2D *textureClip2D = getClipByName(clipName);
    if(textureClip2D) {
        GLUtil::renderQuadWithTexture(x, y, _textureId, _width, _height, textureClip2D->x,
                                      textureClip2D->y, textureClip2D->width, textureClip2D->height, scaleX, scaleY, rotateAngle);
    }
    
}

string Atlas2D::getName() {
    return _name;
}

float Atlas2D::getWidth() {
    return _width;
}

float Atlas2D::getHeight() {
    return _height;
}

float Atlas2D::getQuadWidth(string clipName) {
    TextureClip2D *clip = getClipByName(clipName);
    return  clip ? clip->width : 0;
}

float Atlas2D::getQuadHeight(string clipName) {
    TextureClip2D *clip = getClipByName(clipName);
    return  clip ? clip->height : 0;
}

TextureClip2D* Atlas2D::getClipByName(string clipName) {
    for (int i = 0; i < _textureClips.size(); i++) {
        TextureClip2D *textureClip2D = _textureClips.at(i);
        if(strcmp(textureClip2D->name.c_str(), clipName.c_str()) == 0) {
            return textureClip2D;
        }
    }
    return nullptr;
}

