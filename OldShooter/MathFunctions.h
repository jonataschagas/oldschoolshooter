//
//  MathFunctions.h
//  OldShooter
//
//  Created by jchagas on 24/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__MathFunctions__
#define __OldShooter__MathFunctions__

#include <stdio.h>

class MathFunctions {
public:
    
    static float lerp(float goal, float current, float deltaTime) {
        float difference = goal - current;
        
        if(difference > deltaTime) {
            return current + deltaTime;
        }
        
        if (difference < -deltaTime) {
            return current - deltaTime;
        }
        
        return goal;
    }
    
};

#endif /* defined(__OldShooter__MathFunctions__) */
