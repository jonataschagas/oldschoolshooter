//
//  Explosion.h
//  OldShooter
//
//  Created by jchagas on 20/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Explosion__
#define __OldShooter__Explosion__

#include <stdio.h>
#include "GameObject.h"
#include "Game.h"
#include "Utils.h"

#define EXPLOSION_SCALE 0.8

class Explosion : public GameObject {
public:
    
    Explosion (Game *game, GameObjectType gameObjectType, float x, float y)
    : GameObject(EXPLOSION_SPRITESHEET,
                 "explosion1_0001.png",
                 x,
                 y,
                 EXPLOSION_SCALE,
                 EXPLOSION_SCALE,
                 0.f,
                 gameObjectType,
                 game) {
        _currentFrame = 0;
        _time = 0;
    }
    
    void update(float deltaTime);
private:
    int _currentFrame;
    int _time;
};

#endif /* defined(__OldShooter__Explosion__) */
