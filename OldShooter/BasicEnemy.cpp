//
//  BasicEnemy.cpp
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "BasicEnemy.h"
#include <math.h>

void BasicEnemy::update(float deltaTime) {
    position.x -= ENEMY_SPEED;
    position.y = _length * sinf(_angle) + _initialY;
    _angle -= _angleStepSize;
}