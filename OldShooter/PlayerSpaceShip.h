//
//  PlayerSpaceShip.h
//  OldShooter
//
//  Created by jchagas on 09/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__PlayerSpaceShip__
#define __OldShooter__PlayerSpaceShip__

#include <stdio.h>
#include "AliveGameObject.h"
#include <Carbon/Carbon.h>
#include "Game.h"
#include "Utils.h"
#include "Vector.h"

#define PLAYER_SCALE 0.3
#define PLAYER_HEALTH 100.0F
#define PLAYER_DAMAGE 1.F

class PlayerSpaceShip : public AliveGameObject {
public:
    
    PlayerSpaceShip (Game *game, GameObjectType gameObjectType, float x, float y)
        : AliveGameObject(SPACEGAME_SPRITESHEET,
                     "playerShip2_blue.png",
                     x,
                     y,
                     PLAYER_SCALE,
                     PLAYER_SCALE,
                     90.f,
                     gameObjectType,
                     game,
                     PLAYER_HEALTH,
                     PLAYER_DAMAGE) {
            _extraAmmo = false;
            _extraAmmoTime = 0;
        }
     
    void update(float deltaTime);
    
    void setExtraAmmo();
    
    bool isExtraAmmodOn();
    
    void setShield();
    
    bool isShieldOn();

private:
    bool _extraAmmo;
    long _extraAmmoTime;
    bool _shield;
    long _shieldTime;
    
    Vector _velocity;
    Vector _velocityGoal;
    
    KeyMap _keyStates;
    
    bool isKeyDown( uint16_t vKey );
    
};

#endif /* defined(__OldShooter__PlayerSpaceShip__) */
