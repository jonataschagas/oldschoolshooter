//
//  HealthPack.h
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__HealthPack__
#define __OldShooter__HealthPack__

#include <stdio.h>
#include "GameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"
#include <math.h>

#define SCALE 1
#define SPEED 3

class HealthPack : public GameObject {
public:
    
    HealthPack (Game *game, GameObjectType gameObjectType, float x, float y)
    : GameObject(SPACEGAME_SPRITESHEET,
                 "pill_blue.png",
                 x,
                 y,
                 SCALE,
                 SCALE,
                 90.f,
                 gameObjectType,
                 game) {
    }
    
    
    void update(float deltaTime);
private:
};


#endif /* defined(__OldShooter__HealthPack__) */
