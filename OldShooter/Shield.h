//
//  Shield.h
//  OldShooter
//
//  Created by jchagas on 22/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Shield__
#define __OldShooter__Shield__

#include <stdio.h>
#include "GameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"
#include <math.h>

#define SCALE 1
#define SPEED 3

class Shield : public GameObject {
public:
    
    Shield (Game *game, GameObjectType gameObjectType, float x, float y)
    : GameObject(SPACEGAME_SPRITESHEET,
                 "powerupBlue_shield.png",
                 x,
                 y,
                 SCALE,
                 SCALE,
                 180.f,
                 gameObjectType,
                 game) {
    }
    
    
    void update(float deltaTime);
private:
};


#endif /* defined(__OldShooter__Shield__) */
