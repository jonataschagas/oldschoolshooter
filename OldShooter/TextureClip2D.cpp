//
//  TextureClip2D.cpp
//  OldShooter
//
//  Created by jchagas on 19/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "TextureClip2D.h"


TextureClip2D::TextureClip2D(){}

TextureClip2D::TextureClip2D(string namep, float xp, float yp, float widthp, float heightp) {
    name = namep;
    x = xp;
    y = yp;
    width = widthp;
    height = heightp;
}