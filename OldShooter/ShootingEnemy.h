//
//  ShootingEnemy.h
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__ShootingEnemy__
#define __OldShooter__ShootingEnemy__

#include <stdio.h>

#include "AliveGameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"

#define ENEMY_SCALE 0.3
#define ENEMY_SCALE 0.3
#define ENEMY_SPEED 5
#define ENEMY_SHOOTING_PERIOD 50
#define SHOOTING_ENEMY_HEALTH 3
#define SHOOTING_ENEMY_DAMAGE 10

class ShootingEnemy : public AliveGameObject {
public:
    
    ShootingEnemy (Game *game, GameObjectType gameObjectType, float x, float y)
    : AliveGameObject(SPACEGAME_SPRITESHEET,
                 "enemyBlack1.png",
                 x,
                 y,
                 ENEMY_SCALE,
                 ENEMY_SCALE,
                 90.f,
                 gameObjectType,
                 game,
                SHOOTING_ENEMY_HEALTH,
                SHOOTING_ENEMY_DAMAGE) {
        _speed = ENEMY_SPEED;
        _timeShooting = 0;
    }
    
    
    void update(float deltaTime);
private:
    float _speed;
    float _timeShooting;
};

#endif /* defined(__OldShooter__ShootingEnemy__) */
