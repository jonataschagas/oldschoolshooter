//
//  AliveGameObject.h
//  OldShooter
//
//  Created by jchagas on 25/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__AliveGameObject__
#define __OldShooter__AliveGameObject__

#include <stdio.h>
#include "GameObject.h"
#include "Damageable.h"

class Game;
class AliveGameObject : public GameObject, public Damageable {
public:
    
    AliveGameObject(string atlasName, string defaultClip, float x, float y, float scaleX, float scaleY, float rotateAngle
                    , GameObjectType type, Game *game, float health, float damage);
    
    float getHealth();
    void setDamageToHealth(float damage);
    void setIncreaseHealth(float increase);
    
private:
    float _health;
};



#endif /* defined(__OldShooter__AliveGameObject__) */
