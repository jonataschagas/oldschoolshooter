//
//  Game.h
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Game__
#define __OldShooter__Game__

#include <stdio.h>
#include <vector>
#include "GameObject.h"
#include "AliveGameObject.h"

using namespace std;

typedef struct Star {
    float x;
    float y;
    float r;
    float g;
    float b;
} Star;

class Game {
    
public:
    
    static Game* create(float screen_width, float screen_height);
    
    // creates the game objects based on their type
    void createGameObject(GameObjectType gameObjectType, float x, float y);
    
    void createBullets(GameObjectType gameObjectType, float x, float y, float angle);
    
    void createCollectables();
    
    void render();
    
    // executes all the logic of the game
    void update(float deltaTime);
    
    vector<GameObject*> getGameObjects();
    
    float getScreenHeight();
    float getScreenWidth();
    
    void resetGame();
    void setStartGame();
    
    Game();
    ~Game();
    
private:
    
    float screen_width;
    float screen_height;
    
    vector<Star*> _stars;
    vector<GameObject*> _gameObjects;
    int score;
    bool gameOver = false;
    bool start = false;
    
    // checks if two game objects collided
    bool collide(GameObject *go1, GameObject *go2);
    
    // verifies if the bullet or meteor has hit any enemy
    void checkObjectHitEnemies(GameObject *playerBullet);
    
    // destroys the game objects that are no longer
    // visible on the screen
    void destroyGameObjects();
    
    void checkPlayerCollision(GameObject *player);
    
    void computeCollisionDamage(AliveGameObject *gameObject1, AliveGameObject *gameObject2);
    
    void createEnemies();
    
    void createStars();
    
    void createMeteors(float x, float y, float scale, float angle, bool immediately);
    
    void verifyShield();
    
    void drawHealthBar();
    
    void drawScore();
    
    void drawStartMessage();
    
    void drawGameOver();
    
    void drawText(const char* text, float x, float y);
        
    GameObject* getGameObject(GameObjectType gameObjectType);
    void deleteGameObject(GameObjectType gameObjectType);
    
};

#endif /* defined(__OldShooter__Game__) */
