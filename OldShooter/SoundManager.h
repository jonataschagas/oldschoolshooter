//
//  SoundManager.h
//  OldShooter
//
//  Created by jchagas on 22/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__SoundManager__
#define __OldShooter__SoundManager__

#include <stdio.h>
#include "fmod.hpp"
#include <string>

using namespace std;

class SoundManager{
public:
    static SoundManager* Instance();
    
    void init();
    
    void playSound(string file);

private:
    SoundManager(){};  // Private so that it can  not be called
    SoundManager(SoundManager const&){};             // copy constructor is private
    static SoundManager* m_pInstance;
    FMOD::System     *system;
    FMOD::Sound      *sound1, *sound2, *sound3;
    FMOD::Channel    *channel = 0;
    FMOD_RESULT       result;
    unsigned int      version;
    void             *extradriverdata = 0;
};


#endif /* defined(__OldShooter__SoundManager__) */
