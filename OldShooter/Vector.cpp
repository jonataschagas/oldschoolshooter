//
//  Vector.cpp
//  OldShooter
//
//  Created by jchagas on 17/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "Vector.h"
#include <math.h>


Vector::Vector() {}

Vector::Vector(float x, float y) {
    this->x = x;
    this->y = y;
}

Vector Vector::operator+(const Vector& v) const {
    Vector r;
    
    r.x = x + v.x;
    r.y = y + v.y;
    
    return r;
}

Vector Vector::operator-(const Vector& v) const {
    Vector r;
    
    r.x = x - v.x;
    r.y = y - v.y;
    
    return r;
}


Vector operator-(Point2D a, Point2D b) {
    Vector v2;
    
    v2.x = a.x - b.x;
    v2.y = a.y - b.y;
    
    return v2;
}

Vector Vector::operator*(float s) const {
    
    Vector scaled;

    scaled.x = x * s;
    scaled.y = y * s;
    
    return scaled;
}

Vector Vector::operator/(float s) const {
    
    Vector scaled;
    
    scaled.x = x / s;
    scaled.y = y / s;
    
    return scaled;
}

float Vector::length() const {
    float length;
    
    length = sqrt(x*x + y*y);
    
    return length;
}

float Vector::lengthSqr() const {
    float length;
    
    length = x*x + y*y;
    
    return length;
}

Vector Vector::normalized() const {
    Vector v2;
    v2 = (*this)/length();
    
    return v2;
}