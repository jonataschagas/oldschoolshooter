//
//  GameObject.h
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__GameObject__
#define __OldShooter__GameObject__

#include <stdio.h>
#include "Node.h"

enum GameObjectType {
    PLAYER,
    PLAYER_BULLET,
    PLAYER_DIAGONAL_BULLET,
    ENEMY,
    ENEMY_BULLET,
    EXPLOSION,
    HEALTH,
    EXTRA_AMMO,
    SHIELD,
    PLAYER_SHIELD,
    METEOR
};

class Game;
class GameObject : public Node {
public:

    GameObject(string atlasName, string defaultClip, float x, float y, float scaleX, float scaleY, float rotateAngle
               , GameObjectType type, Game *game);
    
    GameObjectType getType();
    
protected:
    Game *_game;
    GameObjectType _type;
};

#endif /* defined(__OldShooter__GameObject__) */
