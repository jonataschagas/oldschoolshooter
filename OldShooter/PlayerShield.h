//
//  PlayerShield.h
//  OldShooter
//
//  Created by jchagas on 22/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__PlayerShield__
#define __OldShooter__PlayerShield__

#include <stdio.h>
#include "GameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"
#include <math.h>
#include "PlayerSpaceShip.h"

#define SCALE 0.5f
#define SPEED 3


class PlayerShield : public GameObject {
public:
    
    PlayerShield (Game *game, GameObjectType gameObjectType, float x, float y, PlayerSpaceShip* player)
    : GameObject(SPACEGAME_SPRITESHEET,
                 "shield1.png",
                 x,
                 y,
                 SCALE,
                 SCALE,
                 90.f,
                 gameObjectType,
                 game) {
        _player = player;
    }
    
    
    void update(float deltaTime);
private:
    PlayerSpaceShip* _player;
};

#endif /* defined(__OldShooter__PlayerShield__) */
