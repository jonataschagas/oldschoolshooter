//
//  Explosion.cpp
//  OldShooter
//
//  Created by jchagas on 20/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "Explosion.h"
#include <string>
#include <sstream>

void Explosion::update(float deltaTime) {
    if(_currentFrame >= 90) {
        // positions the explosion out of the screen
        // so it will be removed
        position.x = 10000000;
        position.y = 10000000;
        return;
    }

    _currentFrame++;
    std::stringstream sstm;
    if(_currentFrame < 10) {
        sstm << "explosion1_000" << _currentFrame << ".png";
    } else {
        sstm << "explosion1_00" << _currentFrame << ".png";
    }

    setCurrentClip(sstm.str());
}