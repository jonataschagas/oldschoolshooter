//
//  Node.h
//  OldShooter
//
//  Created by jchagas on 09/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Node__
#define __OldShooter__Node__

#include <stdio.h>
#include <string>
#include "Point2D.h"
#include "Atlas2D.h"
#include "Vector.h"
#include <vector>

using namespace std;

class Node {
public:
    
    Node(string atlasName, string defaultClip, float x, float y, float scaleX, float scaleY, float rotateAngle);
    
    ~Node();
    
    // renders the object based on it's properties
    void render();
    
    void rotate(int axis, float degrees);
    void scale(int axis, float percentage);
    
    // pops the pressed keys and process the logic of the
    // object
    virtual void update(float deltaTime);
    
    void addChild(Node *node);
    void removeChild(Node *node);
    
    Point2D position;
    Vector velocity;
    
    float getWidth();
    float getHeight();
    
    void setCurrentClip(string clipName);
    
    string getCurrentClip();
    
private:
    Atlas2D *_atlas2D;
    string _defaultClip;
    vector<string> _clipNames;
    string _currentClip;
    float _scaleX;
    float _scaleY;
    float _rotateAngle;

protected:
    float _width;
    float _height;
};


#endif /* defined(__OldShooter__Node__) */
