//
//  BasicEnemy.h
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__BasicEnemy__
#define __OldShooter__BasicEnemy__

#include <stdio.h>
#include "AliveGameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"

#define ENEMY_SCALE 0.3
#define ENEMY_SCALE 0.3
#define ENEMY_SPEED 5;
#define ENEMY_ANGULAR_SPEED 0.03f
#define ENEMY_LENGTH_SPEED 100f
#define BASIC_ENEMY_HEALTH 1
#define BASIC_ENEMY_DAMAGE 10

class BasicEnemy : public AliveGameObject {
public:
    
    BasicEnemy (Game *game, GameObjectType gameObjectType, float x, float y)
    : AliveGameObject(SPACEGAME_SPRITESHEET,
                 "enemyRed3.png",
                 x,
                 y,
                 ENEMY_SCALE,
                 ENEMY_SCALE,
                 90.f,
                 gameObjectType,
                 game,
                 BASIC_ENEMY_HEALTH,
                 BASIC_ENEMY_DAMAGE) {
        _angle = _game->getScreenWidth() + 10;
        _angleStepSize = 0.03f;
        _length = 50;
        _initialY = y;
    }
    
    
    void update(float deltaTime);
private:
    float _length;
    float _angle;
    float _angleStepSize;
    float _initialY;
};
#endif /* defined(__OldShooter__BasicEnemy__) */
