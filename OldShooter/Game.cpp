//
//  Game.cpp
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include <stdio.h>
#include "Game.h"
#include "PlayerSpaceShip.h"
#include "PlayerBullet.h"
#include "BasicEnemy.h"
#include "ShootingEnemy.h"
#include "EnemyUFO.h"
#include "EnemyBullet.h"
#include "Explosion.h"
#include "HealthPack.h"
#include "ExtraAmmunition.h"
#include "Shield.h"
#include "PlayerShield.h"
#include <time.h>
#include <sstream>
#include <string>
#include "GLUtil.h"
#include <math.h>
#include "Atlas2DManager.h"
#include "Utils.h"
#include "Meteor.h"

#define AMOUNT_STARS 1000
#define SPEED_STARS 2
#define ENEMY_SPAWN_TIME 15
#define METEORS_SPAWN_TIME 750
#define HEALTH_PACK 30
#define COLLECTABLES_SPAWN_TIME 500

#define METEOR_ORIGINAL_SCALE 1

Game::Game(){}
Game::~Game(){}

Game* Game::create(float screen_width, float screen_height) {
    
    Atlas2DManager::loadAtlas(SPACEGAME_SPRITESHEET,
                              "sheet.png",
                              "sheet.xml");
    
    Atlas2DManager::loadAtlas(EXPLOSION_SPRITESHEET,
                              "explosion.png",
                              "explosion.xml");
    
    Game* game = new Game();
    
    game->screen_width = screen_width;
    game->screen_height = screen_height;
    
    PlayerSpaceShip* playerSpaceShip = new PlayerSpaceShip(game,
                                                           GameObjectType::PLAYER,
                                                           game->getScreenWidth() * 0.05f,
                                                           game->getScreenHeight()/2);
    game->_gameObjects.push_back(playerSpaceShip);
    game->gameOver = false;
    game->score = 0;
    
    return game;
}

void Game::createGameObject(GameObjectType gameObjectType, float x, float y) {
    switch (gameObjectType) {
        case GameObjectType::PLAYER_SHIELD : {
            _gameObjects.push_back(new PlayerShield(this, GameObjectType::PLAYER_SHIELD, x, y
                                                    , (PlayerSpaceShip*)getGameObject(GameObjectType::PLAYER)));
        }
        case GameObjectType::ENEMY : {
            int min = 0;
            int max = 2;
            int enemyType = rand() % (max-min + 1) + min;
            
            GameObject* enemy;
            
            switch (enemyType) {
                case 0:
                    enemy = new BasicEnemy(this, GameObjectType::ENEMY, x + 20, y);
                    break;
                case 1:
                    enemy = new EnemyUFO(this, GameObjectType::ENEMY, x, y);
                    break;
                case 2:
                    enemy = new ShootingEnemy(this, GameObjectType::ENEMY, x, y);
                    break;
                default:
                    break;
            }
            
            _gameObjects.push_back(enemy);
            
        }
        case GameObjectType::EXPLOSION : {
            // create enemy
            Explosion *explosion = new Explosion(this,
                                                 GameObjectType::EXPLOSION,
                                                 x,
                                                 y);
            _gameObjects.push_back(explosion);
            break;
        }
        case GameObjectType::HEALTH : {
            // create hp
            HealthPack *health = new HealthPack(this,
                                                 GameObjectType::HEALTH,
                                                 x,
                                                 y);
            _gameObjects.push_back(health);
            break;
        }
        case GameObjectType::EXTRA_AMMO : {
            ExtraAmmunition *extraAmmunition = new ExtraAmmunition(this,
                                                GameObjectType::EXTRA_AMMO,
                                                x,
                                                y);
            _gameObjects.push_back(extraAmmunition);
            break;
        }
        case GameObjectType::SHIELD : {
            _gameObjects.push_back(new Shield(this, GameObjectType::SHIELD,x,y));
            break;
        }

    }
}

void Game::createBullets(GameObjectType gameObjectType, float x, float y, float angle) {
    switch (gameObjectType) {
        case GameObjectType::PLAYER_BULLET : {
            // create bullet
            PlayerBullet* playerBullet = new PlayerBullet(this, GameObjectType::PLAYER_BULLET, x, y, angle);
            _gameObjects.push_back(playerBullet);
            break;
        }
        case GameObjectType::ENEMY_BULLET : {
            // create bullet
            EnemyBullet* enemyBullet = new EnemyBullet(this, GameObjectType::ENEMY_BULLET, x, y);
            _gameObjects.push_back(enemyBullet);
            break;
        }
    }
}

void Game::render() {
    
    if(gameOver) {
        drawGameOver();
        return;
    }
    
    drawScore();
    
    drawHealthBar();
    
    if(!start) {
        drawStartMessage();
    }
    
    // render the stars
    for (int i = 0; i < _stars.size(); i++) {
        Star *star = _stars.at(i);
        GLUtil::renderQuad(star->x, star->y, 0, 1, 1, star->r, star->g, star->b);
    }

    
    // render game objects
    for (int i = 0; i < _gameObjects.size(); i++) {
        _gameObjects.at(i)->render();
    }
    
 }

void Game::resetGame() {
    start = false;
    gameOver = false;
    PlayerSpaceShip* player = (PlayerSpaceShip*)getGameObject(GameObjectType::PLAYER);
    player->position.x = getScreenWidth() * 0.05f;
    player->position.y = getScreenHeight()/2;
    player->setIncreaseHealth(100.F);
    score = 0;
    for(int i = 0; i < _gameObjects.size(); i++) {
        if(_gameObjects.at(i)->getType() != GameObjectType::PLAYER) {
            GameObject *gameObject = _gameObjects.at(i);
            delete gameObject;
        }
    }
    
    for(int i = 0; i < _stars.size(); i++) {
        Star  *star = _stars.at(i);
        delete star;
    }
    
    _gameObjects.clear();
    _stars.clear();
    
    _gameObjects.push_back(player);
    
    printf("reset");
}

void Game::setStartGame() {
    start = true;
}

void Game::drawStartMessage() {
    std::stringstream ss;
    ss << "Use the arrow keys to navigate and press the 'space' key to shoot. Press 'Enter' to start again";
    drawText(ss.str().c_str(), getScreenWidth() * 0.15f, this->getScreenHeight()/2);
}

void Game::update(float deltaTime) {
    
    destroyGameObjects();
    
    checkPlayerCollision(getGameObject(GameObjectType::PLAYER));
    
    for (int i = 0; i < _gameObjects.size(); i++) {
        GameObject *gameObject = _gameObjects.at(i);
        
        // updating all the game objects
        gameObject->update(deltaTime);
        
        if(gameObject->getType() == GameObjectType::PLAYER_BULLET
           || gameObject->getType() == GameObjectType::METEOR) {
            checkObjectHitEnemies(gameObject);
        }
    }
    
    // moves the stars
    for (int i = 0; i < _stars.size(); i++) {
        _stars.at(i)->x -= SPEED_STARS;
    }
    
    if(((PlayerSpaceShip*)getGameObject(GameObjectType::PLAYER))->getHealth() <= 0) {
        gameOver = true;
        drawGameOver();
        return;
    }
    
    
    createStars();
    
    // if game did not start yet
    // dont create enemies and other items
    if(!start) {
        return;
    }
    
    createEnemies();
    
    createCollectables();
    
    createMeteors(0, 0, METEOR_ORIGINAL_SCALE, 0.f, false);
    
    verifyShield();
}

void Game::createEnemies() {
    static int time = 0;
    if (time > ENEMY_SPAWN_TIME) {
        time = 0;
        
        // if there is a meteor
        // we need to make sure the enemies won't crash in the meteor
        // as they are generated
        GameObject *meteor = getGameObject(GameObjectType::METEOR);
        if(meteor) {
            
            int min = 0;
            int max = 0;
            if(rand() % 2 == 2) {
                // above the meteor
                min = meteor->position.y;
                max = getScreenHeight() * 0.95;
            } else {
                // under the meteor
                min = 0;
                max = meteor->getHeight() + meteor->position.y;
            }
            createGameObject(GameObjectType::ENEMY, getScreenWidth(),rand() % (max-min + 1) + min);
        } else {
            createGameObject(GameObjectType::ENEMY, getScreenWidth(), rand() % (int) getScreenHeight() * 0.95);
        }
    }
    time++;
}

void Game::verifyShield() {
    if(((PlayerSpaceShip*)getGameObject(GameObjectType::PLAYER))->isShieldOn() == false) {
        deleteGameObject(GameObjectType::PLAYER_SHIELD);
    }
}

void Game::createMeteors(float x, float y, float scale, float angle, bool immediately) {
    // if using the extra ammo lets not create meteors
    // otherwise there are too many elements in the screen and the performance drops
    if (((PlayerSpaceShip*)getGameObject(GameObjectType::PLAYER))->isExtraAmmodOn()) {
        return;
    }
    
    if(immediately) {
        _gameObjects.push_back(new Meteor(this, GameObjectType::METEOR,
                                          x, y, scale, angle));
    } else {
        static int timeMeteor = 0;
        if (timeMeteor > METEORS_SPAWN_TIME) {
            timeMeteor = 0;
            _gameObjects.push_back(new Meteor(this, GameObjectType::METEOR,
                                              getScreenWidth(), rand() % (int) getScreenHeight() * 0.95,
                                              scale, angle));
        }
        timeMeteor++;
    }
}

void Game::createCollectables() {
    static int timeCollectables = 0;
    if (timeCollectables > COLLECTABLES_SPAWN_TIME) {
        timeCollectables = 0;
        
        int min = 0;
        int max = 2;
        int collectableType = rand() % (max-min + 1) + min;
        
        switch (collectableType) {
            case 0:
                createGameObject(GameObjectType::HEALTH, getScreenWidth(), rand() % (int) getScreenHeight() * 0.95);
                break;
            case 1:
                createGameObject(GameObjectType::EXTRA_AMMO, getScreenWidth(), rand() % (int) getScreenHeight() * 0.95);
                break;
            case 2:
                createGameObject(GameObjectType::SHIELD, getScreenWidth(), rand() % (int) getScreenHeight() * 0.95);
                break;
            default:
                break;
        }
    }
    timeCollectables++;
}

void Game::createStars() {
    
    bool firstTime = _stars.size() == 0;
    
    // creates new stars
    int starsToCreate = AMOUNT_STARS - (int)_stars.size();
    for (int i = 0; i < starsToCreate; i++) {
        Star *star = new Star();

        if(firstTime) {
            // generates the stars in the whole screen
            star->x = rand() % (int) getScreenWidth();
        } else {
            // generaes the stars only at the end of the screen
            star->x = getScreenWidth();
        }
        
        star->r = 1.f;
        star->g = 1.f;
        star->b = 1.f;

        star->y = rand() % (int) getScreenHeight();
        _stars.push_back(star);
    }
}


vector<GameObject*> Game::getGameObjects() {
    return _gameObjects;
}

void Game::checkObjectHitEnemies(GameObject *collidingObject) {
    // detects if there was collision with the enemies
    for (int i = 0; i < _gameObjects.size(); i++) {
        GameObject *gameObjectCollision = _gameObjects.at(i);
        
        if(collide(collidingObject, gameObjectCollision)
           && collidingObject->getType() == GameObjectType::PLAYER_BULLET
           && gameObjectCollision->getType() == GameObjectType::ENEMY) {
            computeCollisionDamage((AliveGameObject*) collidingObject, (AliveGameObject*) gameObjectCollision);
            // creates the explosion
            createGameObject(GameObjectType::EXPLOSION, gameObjectCollision->position.x, gameObjectCollision->position.y);
        } else if(collide(collidingObject, gameObjectCollision)
                  && collidingObject->getType() == GameObjectType::METEOR
                  && gameObjectCollision->getType() == GameObjectType::ENEMY) {

            computeCollisionDamage((AliveGameObject*) collidingObject, (AliveGameObject*) gameObjectCollision);
            // creates the explosion
            createGameObject(GameObjectType::EXPLOSION, gameObjectCollision->position.x, gameObjectCollision->position.y);

            Meteor *hitMeteor = (Meteor*) collidingObject;
            
            if(hitMeteor->getScale() > 0.25f) {
                // creating smaller meteors
                
                float childMetScale = hitMeteor->getScale() * 0.5f;
                
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, 30.f, true);
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, -30.f, true);
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, 90.f, true);
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, -90.f, true);

            }
            score +=1;

        } else if(collide(collidingObject, gameObjectCollision)
                  && collidingObject->getType() != GameObjectType::METEOR
                  && gameObjectCollision->getType() == GameObjectType::METEOR) {
            Meteor *hitMeteor = (Meteor*) gameObjectCollision;
            
            computeCollisionDamage((AliveGameObject*) collidingObject, (AliveGameObject*) gameObjectCollision);
            
            if(hitMeteor->getScale() > 0.25f) {
                
                float childMetScale = hitMeteor->getScale() * 0.5f;
                
                // creating 3 smaller meteors
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, 30.f, true);
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, -30.f, true);
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, 90.f, true);
                createMeteors(gameObjectCollision->position.x,
                              gameObjectCollision->position.y, childMetScale, -90.f, true);
            }
        }
    }

}

void Game::checkPlayerCollision(GameObject *player) {
    
    PlayerSpaceShip *playerSpaceShip = (PlayerSpaceShip*) player;
    
    for (int i = 0; i < _gameObjects.size(); i++) {
        GameObject *gameObjectCollision = _gameObjects.at(i);
        
        if(AliveGameObject* aliveObj = dynamic_cast<AliveGameObject*>(gameObjectCollision)) {
            
            if(collide(player, gameObjectCollision) &&
                (gameObjectCollision->getType() == GameObjectType::ENEMY ||
                 gameObjectCollision->getType() == GameObjectType::ENEMY_BULLET ||
                 gameObjectCollision->getType() == GameObjectType::METEOR)) {
               
               //printf("collision with %s\n", gameObjectCollision->getCurrentClip().c_str());
                    
               computeCollisionDamage((AliveGameObject*)player, aliveObj);
               // creates the explosion
               createGameObject(GameObjectType::EXPLOSION, player->position.x, player->position.y);
           }
        } else if(collide(player, gameObjectCollision) && gameObjectCollision->getType() == GameObjectType::HEALTH) {
            // destroy health pack
            _gameObjects.erase(_gameObjects.begin() + i);
            delete gameObjectCollision;
            playerSpaceShip->setIncreaseHealth(HEALTH_PACK);
        } else if(collide(player, gameObjectCollision) && gameObjectCollision->getType() == GameObjectType::EXTRA_AMMO) {
            // destroy collectable
            _gameObjects.erase(_gameObjects.begin() + i);
            delete gameObjectCollision;
            // enables the extra ammo mode
            ((PlayerSpaceShip*)player)->setExtraAmmo();
        } else if(collide(player, gameObjectCollision) && gameObjectCollision->getType() == GameObjectType::SHIELD) {
            // destroy collectable
            _gameObjects.erase(_gameObjects.begin() + i);
            delete gameObjectCollision;
            // enables the extra ammo mode
            ((PlayerSpaceShip*)player)->setShield();
            // creates the shield
            createGameObject(GameObjectType::PLAYER_SHIELD, 0, 0);
        }
    }
}

void Game::computeCollisionDamage(AliveGameObject *gameObject1, AliveGameObject *gameObject2) {
    gameObject1->setDamageToHealth(gameObject2->getDamage());
    gameObject2->setDamageToHealth(gameObject1->getDamage());
}

void Game::drawScore() {
    // converts int to string
    std::stringstream ss;
    ss << "Score: " << score;
    drawText(ss.str().c_str(), getScreenWidth() * 0.5f, getScreenHeight() * 0.95);
}

void Game::drawGameOver() {
    std::stringstream ss;
    ss << "Game Over! You have Destroyed " << score << " enemy ships. Press 'enter' to start again";
    drawText(ss.str().c_str(), getScreenWidth() * 0.3f, getScreenHeight()/2);
}

void Game::drawText(const char* text, float x, float y) {
    // converts int to string
    GLUtil::drawText(text, x, y);
}

void Game::drawHealthBar() {
    
    PlayerSpaceShip *player = (PlayerSpaceShip*)getGameObject(GameObjectType::PLAYER);
    
    // draws the border of the health bar
    GLUtil::color(0.f, 0.f, 0.8f);
    GLUtil::renderQuad(getScreenWidth()/2 * 0.08, getScreenHeight()/2 * 0.94, 0,
                       (getScreenWidth() * 0.6 - getScreenWidth() * 0.2) ,
                       (getScreenHeight() * 0.99 - getScreenHeight() * 0.95));
    
    float start_of_bar = getScreenWidth()/2 * 0.09;
    float end_of_bar = getScreenWidth()/2 * 0.77f;
    float length_of_bar = end_of_bar - start_of_bar;
    
    float whole_bar = start_of_bar + length_of_bar * player->getHealth()/100;
    // draws the health bar
    GLUtil::color(1.f, 0.f, 0.f);
    GLUtil::renderQuad(getScreenWidth()/2 * 0.09, getScreenHeight()/2 * 0.95, 0,
                       whole_bar ,
                       (getScreenHeight() * 0.99 - getScreenHeight() * 0.97));
}

bool Game::collide(GameObject *go1, GameObject *go2) {
    
    float r1Top = go1->position.y;
    float r1Bot = go1->position.y + go1->getHeight();

    float r2Top = go2->position.y;
    float r2Bot = go2->position.y + go2->getHeight();
    
    float r1Lef = go1->position.x;
    float r1Rig = go1->position.x + go1->getWidth();
    
    float r2Lef = go2->position.x;
    float r2Rig = go2->position.x + go2->getWidth();
    
    return ! (
         (r1Bot < r2Top) ||
         (r1Top > r2Bot) ||
         (r1Lef > r2Rig) ||
         (r1Rig < r2Lef) );
}

void Game::destroyGameObjects() {
    
    // erases game objects out of the screens
    for (int i = 0; i < _gameObjects.size(); i++) {
        GameObject *gameObject = _gameObjects.at(i);
        
        if(gameObject->position.x > (screen_width + 50) || gameObject->position.x < 0 ||
            gameObject->position.y > screen_height || gameObject->position.y < 0) {
            // destroy object
            _gameObjects.erase(_gameObjects.begin() + i);
            delete gameObject;
        }
    }
    
    // deletes game objects that health lower than 0
    for (int i = 0; i < _gameObjects.size(); i++) {
        if(AliveGameObject* aliveObj = dynamic_cast<AliveGameObject*>(_gameObjects.at(i)) ) {
            
            if(aliveObj->getHealth() <= 0 && aliveObj->getType() != GameObjectType::PLAYER) {
                if(aliveObj->getType() == GameObjectType::ENEMY) {
                    score++;
                }
                _gameObjects.erase(_gameObjects.begin() + i);
                delete aliveObj;
            }
        }
    }
    
    // erases stars out of the screen
    for (int i = 0; i < _stars.size(); i++) {
        Star *star = _stars.at(i);
        
        if(star->x > screen_width || star->x < 0 ||
           star->y > screen_height || star->y < 0) {
            // destroy object
            _stars.erase(_stars.begin() + i);
            delete star;
        }
    }
}

GameObject* Game::getGameObject(GameObjectType gameObjectType) {
    for(int i = 0; i < _gameObjects.size(); i++) {
        if(_gameObjects.at(i)->getType() == gameObjectType) {
            return _gameObjects.at(i);
        }
    }
    return nullptr;
}

void Game::deleteGameObject(GameObjectType gameObjectType) {
    for(int i = 0; i < _gameObjects.size(); i++) {
        if(_gameObjects.at(i)->getType() == gameObjectType) {
            GameObject *gameObject = _gameObjects.at(i);
            _gameObjects.erase(_gameObjects.begin() + i);
            delete gameObject;
        }
    }
}

float Game::getScreenHeight() {
    return screen_height;
}
float Game::getScreenWidth() {
    return screen_width;
}

