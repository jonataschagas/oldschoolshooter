//
//  PlayerBullet.cpp
//  OldShooter
//
//  Created by jchagas on 10/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "PlayerBullet.h"
#include <math.h>

#define PLAYER_BULLET_SPEED 8.f

void PlayerBullet::update(float deltaTime) {
    // _angle * M_PI / 180 -> transform from degrees to radians
    
    position.x += PLAYER_BULLET_SPEED * cos( _angle * M_PI / 180);
    position.y += PLAYER_BULLET_SPEED * sin( _angle * M_PI / 180);
    
}
