//
//  Vector.h
//  OldShooter
//
//  Created by jchagas on 17/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Vector__
#define __OldShooter__Vector__

#include <stdio.h>
#include "Point2D.h"

class Vector {
public:
    
    Vector(float x, float y);
    Vector();
    
    float length() const;
    float lengthSqr() const;
    
    Vector operator+(const Vector& v) const;
    Vector operator-(const Vector& v) const;
    
    Vector operator*(float s) const;
    Vector operator/(float s) const;
    
    Vector normalized() const;
    
    float x,y;
};

#endif /* defined(__OldShooter__Vector__) */
