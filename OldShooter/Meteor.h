//
//  Meteor.h
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Meteor__
#define __OldShooter__Meteor__

#include <stdio.h>
#include "AliveGameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"

#define METEOR_HEALTH 1
#define METEOR_DAMAGE 10

class Meteor : public AliveGameObject {
public:
    
    Meteor (Game *game, GameObjectType gameObjectType, float x, float y, float scale, float angle)
    : AliveGameObject(SPACEGAME_SPRITESHEET,
                 "meteorBrown_big3.png",
                 x,
                 y,
                 scale,
                 scale,
                 0.f,
                 gameObjectType,
                 game,
                 METEOR_HEALTH,
                 METEOR_DAMAGE
            ) {
        _angle = angle;
        _scale = scale;
    }
    
    float getScale();
    
    void update(float deltaTime);
private:
    float _angle;
    float _scale;
};

#endif /* defined(__OldShooter__Meteor__) */
