//
//  EnemyUFO.h
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__EnemyUFO__
#define __OldShooter__EnemyUFO__

#include <stdio.h>
#include "AliveGameObject.h"
#include <stdlib.h>
#include "Game.h"
#include "Utils.h"
#include <math.h>

#define ENEMY_SCALE 0.3
#define ENEMY_SCALE 0.3
#define ENEMY_SPEED 15
#define ENEMY_UFO_HEALTH 1
#define ENEMY_UFO_DAMAGE 10

class EnemyUFO : public AliveGameObject {
public:
    
    EnemyUFO (Game *game, GameObjectType gameObjectType, float x, float y)
        : AliveGameObject(SPACEGAME_SPRITESHEET,
                 "ufoRed.png",
                 x,
                 y,
                 ENEMY_SCALE,
                 ENEMY_SCALE,
                 0.f,
                 gameObjectType,
                 game,
                ENEMY_UFO_HEALTH,
                ENEMY_UFO_DAMAGE) {}
    
    void update(float deltaTime);

};
#endif /* defined(__OldShooter__EnemyUFO__) */
