//
//  Utils.h
//  OldShooter
//
//  Created by jchagas on 19/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#ifndef __OldShooter__Utils__
#define __OldShooter__Utils__

#include <stdio.h>
#include <string>
#include <fstream>
#include <streambuf>
#include <vector>

using namespace std;

#define SPACEGAME_SPRITESHEET "spacegame_spritesheet"
#define EXPLOSION_SPRITESHEET "explosion_spritesheet"

class Utils {
public:
    
    static string readFileContents(string path) {
        ifstream t(path);
        string str((istreambuf_iterator<char>(t)),
                        istreambuf_iterator<char>());
        return str;
    }
    
    static void split(const string& s, char c,
               vector<string>& v) {
        string::size_type i = 0;
        string::size_type j = s.find(c);
        
        while (j != string::npos) {
            v.push_back(s.substr(i, j-i));
            i = ++j;
            j = s.find(c, j);
            
            if (j == string::npos)
                v.push_back(s.substr(i, s.length()));
        }
    }
    
    static void loadFileMemory(const char *name, void **buff, int *length)
    {
        FILE *file = fopen(name, "rb");
        
        fseek(file, 0, SEEK_END);
        long len = ftell(file);
        fseek(file, 0, SEEK_SET);
        
        void *mem = malloc(len);
        fread(mem, 1, len, file);
        
        fclose(file);
        
        *buff = mem;
        *length = len;
    }
    
};

#endif /* defined(__OldShooter__Utils__) */
