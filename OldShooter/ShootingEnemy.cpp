//
//  ShootingEnemy.cpp
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "ShootingEnemy.h"

void ShootingEnemy::update(float deltaTime) {
    position.x -= _speed;
    if(_timeShooting > ENEMY_SHOOTING_PERIOD) {
        _game->createBullets(GameObjectType::ENEMY_BULLET, position.x, position.y + 8,0.f);
        _timeShooting = 0;
    }
    _timeShooting +=1;
}