//
//  Meteor.cpp
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "Meteor.h"
#include <math.h>

#define METEOR_SPEED 1

void Meteor::update(float deltaTime) {
    // _angle * M_PI / 180 -> transform from degrees to radians
    
    position.x -= METEOR_SPEED * cos( _angle * M_PI / 180);
    position.y -= METEOR_SPEED * sin( _angle * M_PI / 180);
}

float Meteor::getScale() {
    return _scale;
}