//
//  ExtraAmmunition.cpp
//  OldShooter
//
//  Created by jchagas on 21/05/2015.
//  Copyright (c) 2015 Jonatas Chagas. All rights reserved.
//

#include "ExtraAmmunition.h"

void ExtraAmmunition::update(float deltaTime) {
    position.x -= SPEED;
}

float ExtraAmmunition::getWidth() {
    return getWidth() * 2;
}

float ExtraAmmunition::getHeight() {
    return getHeight() * 2;
}